import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    api_Url: 'http://api.themoviedb.org/3/movie/now_playing',
    api_Key: 'ebea8cfca72fdff8d2624ad7bbf78e4c',
    page: 1,
    total_pages: null,
    movies: null,
    moviePopup: false,

    indexSelectedMovie: null,
    selectedMovie: null
  },
  mutations: {
    UPDATE_MOVIES (state, res) {
      res = JSON.parse(res)
      state.total_pages = res.total_pages
      state.movies = res.results
    }
  },
  actions: {
    getDataMovies (context) {
      var xhr = new XMLHttpRequest()

      xhr.open('GET', context.state.api_Url + '?api_key=' + context.state.api_Key + '&page=' + context.state.page, false)

      xhr.send()

      if (xhr.status !== 200) {
        alert(xhr.status + ': ' + xhr.statusText)
      } else {
        context.commit('UPDATE_MOVIES', xhr.responseText)
      }
    }
  }
})
